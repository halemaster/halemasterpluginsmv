# README #

This README documents how to use the source for developing the project using Gradle.

### How do I get set up? ###

* Gradle Tasks are somewhat self explanatory. 
* TestProject tasks are for handling an RPG Maker MV project you store in the development folder called TestProject.
* * Use the lib folder for any other plugins you want to be used in the TestProject.

### Contribution guidelines ###

* First, all development should be done on a secondary branch.
* Otherwise, using Code Snippets above and inviting someone to code review is standard (unless you are me, which I get to cause I own the project).

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact