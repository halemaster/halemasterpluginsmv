//=============================================================================
// Halemaster Plugins - Reaction
// Halemaster_Reaction.js
//=============================================================================

var Imported = Imported || {};
Imported.Halemaster_Reaction = true;

var Halemaster = Halemaster || {};
Halemaster.Reaction = Halemaster.Reaction || {};

//=============================================================================
 /*:
 * @plugindesc This plugin allows states, actors, classes, enemies, and 
 * equipment to allow you to react to damaging attacks.
 * @author Halemaster
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * Reactions are skills that are used reflexively when a character is damaged.
 * They have some amount of percentage that the action will happen, which is
 * defined via tags on states, actors, classes, enemies, and equipment.
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * Actor, Class, State, Weapon, Armor, Enemy Notetags:
 *   <Reaction: x, y%>
 *   Allows the character to use skill with id x y% of the time when they take
 *   damage from an attack.
 */
//=============================================================================

Halemaster.Reaction.DataManager_isDatabaseLoaded = DataManager.isDatabaseLoaded;
DataManager.isDatabaseLoaded = function() {
    if (!Halemaster.Reaction.DataManager_isDatabaseLoaded.call(this)) return false;
		this.processReactionNotetags($dataClasses);
		this.processReactionNotetags($dataWeapons);
		this.processReactionNotetags($dataArmors);
		this.processReactionNotetags($dataEnemies);
		this.processReactionNotetags($dataActors);
		this.processReactionNotetags($dataStates);
		return true;
};

DataManager.processReactionNotetags = function(group) {
	var note1 = /<(?:REACTION|reaction):[ ](\d+),[ ](\d+)%>/i;
	for (var n = 1; n < group.length; n++) {
		var obj = group[n];
		var notedata = obj.note.split(/[\r\n]+/);

		obj.reactionTypes = []; // always generate a new reactionTypes array.
		obj.reactionPercent = [];

		for (var i = 0; i < notedata.length; i++) {
			var line = notedata[i];
			if (line.match(note1)) {
				obj.reactionTypes.push(parseInt(RegExp.$1));
				obj.reactionPercent.push(parseInt(RegExp.$2));
			}
		}
	}
};

Halemaster.Reaction.Game_Action_executeHpDamage = Game_Action.prototype.executeHpDamage;
Game_Action.prototype.executeHpDamage = function(target, value) {
    if (value > 0 && target.reactTypes && target.reactPercent) { // for being able to have "on take damage" effects
		// react with battler
		reactT = target.reactTypes();
		reactP = target.reactPercent();
		for(var j = 0; j < reactT.length; j++) {
			var chance = reactP[j] / 100.0;
			if(Math.random() <= chance) {
				target.forceReaction(reactT[j], this.subject().index());
				BattleManager.forceAction(target);
			}
		}
    }
    Halemaster.Reaction.Game_Action_executeHpDamage.call(this, target, value);
};

Game_Battler.prototype.forceReaction = function(skillId, targetIndex) {
    var action = new Game_Action(this, true);
    action.setSkill(skillId);
    if (targetIndex === -2) {
        action.setTarget(this._lastTargetIndex);
    } else if (targetIndex === -1) {
        action.decideRandomTarget();
    } else {
        action.setTarget(targetIndex);
    }
    this._actions.unshift(action);
};

Game_Actor.prototype.reactTypes = function() {
	var array = [];
    array = array.concat(this.actor().reactionTypes);
    array = array.concat(this.currentClass().reactionTypes);
    for (var i = 0; i < this.equips().length; ++i) {
      var equip = this.equips()[i];
	  if(equip) {
		array = array.concat(equip.reactionTypes);
	  }
    }
	for (var i = 0; i < this._states.length; ++i) {
	  var stateId = this._states[i];
	  var state = $dataStates[stateId];
	  if (!state) continue;
	  array.push(state.reactionTypes);
	}
    return array;
};

Game_Actor.prototype.reactPercent = function() {
	var array = [];
	array = array.concat(this.actor().reactionPercent);
    array = array.concat(this.currentClass().reactionPercent);
    for (var i = 0; i < this.equips().length; ++i) {
      var equip = this.equips()[i];
	  if(equip) {
		array = array.concat(equip.reactionPercent);
	  }
    }
	for (var i = 0; i < this._states.length; ++i) {
	  var stateId = this._states[i];
	  var state = $dataStates[stateId];
	  if (!state) continue;
	  array.push(state.reactionPercent);
	}
    return array;
};

Game_Enemy.prototype.reactTypes = function() {
	var array = [];
    array = array.concat(this.enemy().reactionTypes);
	for (var i = 0; i < this._states.length; ++i) {
	  var stateId = this._states[i];
	  var state = $dataStates[stateId];
	  if (!state) continue;
	  array.push(state.reactionTypes);
	}
    return array;
};

Game_Enemy.prototype.reactPercent = function() {
	var array = [];
	array = array.concat(this.enemy().reactionPercent);
	for (var i = 0; i < this._states.length; ++i) {
	  var stateId = this._states[i];
	  var state = $dataStates[stateId];
	  if (!state) continue;
	  array.push(state.reactionPercent);
	}
    return array;
};